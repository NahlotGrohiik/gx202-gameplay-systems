﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Card : MonoBehaviour
{
    public bool placed;
    public int id;
    public string cardName;
    public int costOfCard;
    public int power;
    public string cardDescription;
    public int shield;
   
   public Card ()
   {

   }

   public Card (int ID, string CardName, int Cost, int Power, string CardDescription, int Shield)
   {
       id = ID;
       cardName = CardName;
       costOfCard = Cost;
       power = Power;
       cardDescription = CardDescription;
       shield = Shield;
   }
}
