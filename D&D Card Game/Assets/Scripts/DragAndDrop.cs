﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    private RectTransform _rectTransform;
    [SerializeField] private Canvas _canvas;
    private CanvasGroup canvasGroup;

    private void Awake() 
    {
        _rectTransform = GetComponent <RectTransform> ();
        canvasGroup = GetComponent <CanvasGroup>();
    }
     public void OnBeginDrag (PointerEventData eventData)
   {
        Debug.Log("OnBeginDrag");
        canvasGroup.blocksRaycasts = false;
   }

    public void OnDrag (PointerEventData eventData)
   {
       Debug.Log("OnDrag");
       _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
   }

       public void OnEndDrag (PointerEventData eventData)
   {
       Debug.Log("OnEndDrag");
       canvasGroup.blocksRaycasts = true;
   }
   
   public void OnPointerDown (PointerEventData eventData)
   {
       Debug.Log("OnPointerDown");
   }

    public void OnDrop (PointerEventData eventData)
    {

    }
}
