﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneChanger : MonoBehaviour
{
    
    public void StartGame () {
        SceneManager.LoadScene ("GameScene");
        Debug.Log("Loads Game");
    }
    public void InfoScreen (){
        SceneManager.LoadScene ("HelpMenu");
        Debug.Log("Loads Info");
    }
    public void LoadMainMenu (){
        SceneManager.LoadScene ("MainMenu");
        Debug.Log("Goes back to main menu");
    }
    public void QuitGame () {
        Application.Quit ();
        Debug.Log("YEAH IT WORKED");
    }

     public void WinningHelp (){
        SceneManager.LoadScene ("Winning");
        Debug.Log("Loads Winning help screen");
    }

     public void GeneralHelp (){
        SceneManager.LoadScene ("General");
        Debug.Log("Loads General Help screen");
    }
}
