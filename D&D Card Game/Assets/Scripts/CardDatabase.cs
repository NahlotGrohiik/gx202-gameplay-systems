﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDatabase : MonoBehaviour
{
  public static List <Card > cardList = new List<Card> ();

   private void Awake() 
   {
       cardList.Add (new Card (0, "Cat", 0, 0, "Not a big threat, but still hurts", 1));
       cardList.Add (new Card (1, "Wolf", 0, 0, "A wolf. Best when in a pack", 5));
       cardList.Add (new Card (2, "Goblin", 0, 0, "An ugly pickpoket", 3));
       cardList.Add (new Card (3, "Human Soldier", 0, 0, "A dedicated soldier", 5));
       cardList.Add (new Card (4, "Elf", 0, 0, "An agile spirit", 5));
       cardList.Add (new Card (5, "Orc", 0, 0, "A dumb tank on legs", 7));
       cardList.Add (new Card (6, "Giant Spider", 0, 0, "A creepy stalker", 8));
       cardList.Add (new Card (7, "Crag Cat", 0, 0, "A silent hunter", 7));
       cardList.Add (new Card (8, "Minotaur", 0, 0, "A ruthless beats", 9));
       cardList.Add (new Card (9, "Dragon", 0, 0, "An Ancient being", 10));
   }
}
